resource "aws_instance" "amb-prod" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name = "DevOps-Bootcamp"
  security_groups = ["allow_ssh","allow_http", "allow_egress"]
  user_data = file("script.sh")

  tags = {
    Name = "teste-gitlab"
  }
}